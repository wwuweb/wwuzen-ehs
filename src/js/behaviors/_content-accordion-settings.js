(function ($, Drupal, window, document, undefined) {
  $(document).ready(function() {
    $( ".ui-accordion" ).accordion({
      collapsible: true,
      active: false
    });
  });
})(jQuery, Drupal, this, this.document);
